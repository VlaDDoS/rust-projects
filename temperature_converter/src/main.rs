use std::io;

fn main() {
    println!("Введите температуру, которую хотите конвертировать");

    let user_value = get_user_num();

    let t_c = 5.0 / 9.0 * (user_value - 32.0);
    let t_f = 9.0 / 5.0 * user_value + 32.0;

    println!("----------------------------");

    println!("{}℃ = {}℉", user_value, t_f);
    println!("{}℉ = {}℃", user_value, t_c);
}

fn get_user_num() -> f64 {
    let user_value = get_user_value();

    let num_value = user_value
        .trim()
        .parse::<f64>()
        .expect("Не удалось конвертировать");

    return num_value;
}

fn get_user_value() -> String {
    let mut user_value = String::new();

    io::stdin()
        .read_line(&mut user_value)
        .expect("Введены не корректные значения");

    return user_value;
}
