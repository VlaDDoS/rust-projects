use std::io;

fn main() {
    println!("Фибоначчи генератор");
    println!("Введите `q` что бы выйти из программы");

    loop {
        let mut num = String::new();

        println!("\nВведите натуральное число:");

        io::stdin()
            .read_line(&mut num)
            .expect("Ошибка при получении значения");

        if num.trim() == "q" {
            break;
        }

        let num = match num.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("{}", get_fib_iterative(num));
    }
}

// fn get_fib_recursive(num: u64) -> u64 {
//     return match num {
//         0 => 1,
//         1 => 1,
//         _ => get_fib_recursive(num - 1) + get_fib_recursive(num - 2),
//     };
// }

fn get_fib_iterative(num: u64) -> u64 {
    let mut first_num: u64;
    let mut second_num: u64 = 0;
    let mut current_num: u64 = 1;

    let mut idx: u64 = 1;

    while idx < num {
        first_num = second_num;
        second_num = current_num;
        current_num = first_num + second_num;

        idx = idx + 1;
    }

    return current_num;
}
