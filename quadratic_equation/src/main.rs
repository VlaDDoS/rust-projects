use std::io;
use std::process;

fn main() {
    let mut a_str = String::new();
    let mut b_str = String::new();
    let mut c_str = String::new();

    user_input('a', &mut a_str);
    user_input('b', &mut b_str);
    user_input('c', &mut c_str);

    let a: f64 = convert_to_f64(a_str);
    let b: f64 = convert_to_f64(b_str);
    let c: f64 = convert_to_f64(c_str);

    let d: f64 = (b * b) - 4.0 * (a * c);

    print_result(a, b, d)
}

fn user_input(key: char, value: &mut String) {
    println!("Решить квадратное уравнение");
    println!("Введите {}: ", key);

    io::stdin()
        .read_line(value)
        .expect("Не удалось прочитать строку");

    if value.trim().is_empty() {
        println!("ВВЕДИТЕ ЧИСЛО!");
        user_input(key, value);
    }
}

fn convert_to_f64(source_str: String) -> f64 {
    let input = source_str.trim();

    return match input.parse::<f64>() {
        Ok(num) => num,
        Err(err) => {
            println!("Вводите только числа, попробуйте ещё раз, {}", err);
            process::exit(1);
        }
    };
}

fn print_result(a: f64, b: f64, d: f64) {
    if d == 0.0 {
        let x = (-b) / (2.0 * a);

        if x.is_nan() {
            println!("Корней не существует!");
            return;
        }

        println!("РЕШЕНО\nЕсть 1 корень\nD = 0\nx = {}", x);
        return;
    }

    if d > 0.0 {
        let x1 = ((-b) + d.sqrt()) / (2.0 * a);
        let x2 = ((-b) - d.sqrt()) / (2.0 * a);

        println!("РЕШЕНО\nЕсть 2 корня\nD = {}\nx1 = {}\nx2 = {}", d, x1, x2);
        return;
    }

    if d < 0.0 {
        println!("Корней не существует!\nD < 0\nD = {}", d);
        return;
    }
}
